<?php

/**
*
* @package BB3Hide
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

namespace ppk\bb3hide\event;

/**
 * @ignore
 */
if(!defined('IN_PHPBB'))
{
	exit;
}

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class listener implements EventSubscriberInterface
{
	/** @var \phpbb\template\template */
	protected $template;

	/** @var \phpbb\config\config */
	protected $config;

	/** @var \phpbb\user */
	protected $user;

	/** @var \phpbb\auth\auth */
	protected $auth;

	/** @var \phpbb\db\driver\driver_interface */
	protected $db;

	/** @var \phpbb\request\request_interface */
	protected $request;

	/** @var string phpEx */
	protected $php_ext;

	/** @var string phpBB root path */
	protected $root_path;

	/** @var \phpbb\extension\manager */
	protected $phpbb_extension_manager;

	protected $is_admod;
	protected $ignore_groups;
	protected $posters_groups;
	protected $posters_defgroups;
	protected $posters_posts;
 	protected $posts_data;
 	protected $posts_posters;
	protected $register_link;
	protected $is_quickreply;
	protected $hide_search_ary;
	protected $show_search_ary;
	protected $is_preview;

	public function __construct(\phpbb\template\template $template, \phpbb\config\config $config, \phpbb\user $user, \phpbb\auth\auth $auth, \phpbb\db\driver\driver_interface $db, \phpbb\request\request_interface $request, $php_ext, $root_path, \phpbb\extension\manager $phpbb_extension_manager)
	{
		$this->template = $template;
		$this->config = $config;
		$this->user = $user;
		$this->auth = $auth;
		$this->db = $db;
		$this->request = $request;
		$this->php_ext = $php_ext;
		$this->root_path = $root_path;
		$this->phpbb_extension_manager = $phpbb_extension_manager;

		$this->posters_groups='';
		$this->posters_defgroups='';
		$this->posters_posts='';
		$this->posts_data=array();
		$this->posts_posters=array();
		$this->register_link='';
		$this->hide_search_ary=array();
		$this->show_search_ary='';
		$this->is_preview=false;

//		$this->is_quickreply=$this->phpbb_extension_manager->is_enabled('boardtools/quickreply') && $this->config['qr_full_quote'] ? true : false;
	}

	static public function getSubscribedEvents()
	{
		return array(
			//подключение языковых файлов
			'core.user_setup'							=> 'bb3hide_add_lang',
			'core.user_setup_after'							=> 'bb3hide_init',

			//обработка текста для отображения, после обработки системных бб-кодов /includes/functions_content.php 460 generate_text_for_display() -> /viewtopic.php 1550
			'core.modify_text_for_display_after'		=> 'bb3hide_displaytext',

			'core.viewtopic_modify_post_data' => 'bb3hide_get_posts_data',
			'core.mcp_topic_modify_post_data' => 'bb3hide_get_posts_data',
			'core.search_modify_rowset' => 'bb3hide_get_posts_data',
			'core.search_modify_post_row' => 'bb3hide_displaytext',
			'core.search_get_posts_data' => 'bb3hide_get_topic_ids',
			'core.topic_review_modify_post_list' => 'bb3hide_get_posts_data',
// 			'core.viewtopic_modify_post_row'            => array('bb3hide_quickreply', -3),

			'core.decode_message_before' => 'bb3hide_decode_message',

			'core.posting_modify_message_text' => 'bb3hide_check_preview',
			'core.modify_format_display_text_after' => 'bb3hide_preview',

			//изменение бб-кода добавленного через адм. раздел /includes/functions_display.php 1058 display_custom_bbcodes()
			'core.display_custom_bbcodes_modify_row'				=> 'bb3hide_bbtag',
		);
	}

	public function bb3hide_get_topic_ids($event)
	{
		$sql_array=$event['sql_array'];

		$sql = 'SELECT p.topic_id, p.post_id, p.poster_id
			FROM ' . POSTS_TABLE . ' p, '.FORUMS_TABLE .' f
			WHERE p.forum_id=f.forum_id AND ' . $sql_array['WHERE'];
		$result = $this->db->sql_query($sql);
		while($sql_row = $this->db->sql_fetchrow($result))
		{
			$this->posts_posters[$sql_row['poster_id']] = $sql_row['poster_id'];
		}
		$this->db->sql_freeresult($result);
		$this->posts_posters[$this->user->data['user_id']]=$this->user->data['user_id'];
	}
// 	public function bb3hide_quickreply($event)
// 	{
// 		$post_row = $event['post_row'];
//
// 		if ($this->is_quickreply && isset($post_row['DECODED_MESSAGE']))
// 		{
			//bbcode все совпадения и сохранения
// 			$hide_search_ary=$this->bb3hide_get_hide_bbcodes('bbcode');
//
// 			$post_row['DECODED_MESSAGE'] = preg_replace($hide_search_ary, $this->user->lang['BB3HIDE_QUOTE'], $post_row['DECODED_MESSAGE']);
//
// 			$event['post_row']=$post_row;
//
// 		}
// 	}

	public function bb3hide_get_posts_data($event)
	{
		if(isset($event['show_results']))
		{
			if($event['show_results']=='topics')
			{
				return false;
			}
		}

		$rowset=$event['rowset'];
// 		$topic_id=isset($event['topic_id']) ? $event['topic_id'] : 0;

		foreach($rowset as $post_count => $row)
		{
			if($row['bbcode_uid'])
			{
				$poster_id = isset($row['user_id']) ? $row['user_id'] : (isset($row['poster_id']) ? $row['poster_id'] : 2);
				if(!isset($row['forum_id']))
				{
					$row['forum_id']=0;
				}
				$this->posts_data[$row['bbcode_uid']]=array($poster_id, $row['forum_id']);
				$this->posts_posters[$poster_id]=$poster_id;
			}

		}
		$this->posts_posters[$this->user->data['user_id']]=$this->user->data['user_id'];
	}

	public function bb3hide_check_preview($event)
	{
		$preview=$event['preview'];

		if($preview)
		{
			$this->is_preview=true;
		}
	}

	public function bb3hide_decode_message($event)
	{
		$mode=$this->request->variable('mode', '');
		$action=$this->request->variable('action', '');

		if(!$this->is_preview && (in_array($mode, array('quote', 'reply')) || $action=='quotepost'))
		{
			$message=$event['message_text'];

			//message все совпадения, сохранение только текста
			$hide_search_ary = $this->bb3hide_get_hide_bbcodes('message');

			$message = preg_replace($hide_search_ary, $this->user->lang['BB3HIDE_QUOTE'], $message);

			$event['message_text']=$message;

		}
		$this->is_preview=false;
	}

	public function bb3hide_preview($event)
	{
		$text=$event['text'];

// 		if ($this->is_quickreply)
// 		{
			//bbcode все совпадения и сохранения
// 			$hide_search_ary=$this->bb3hide_get_hide_bbcodes('bbcode');
// 		}
// 		else
// 		{
			///html все совпадения, сохранение только текста
			$hide_search_ary=$this->bb3hide_get_hide_bbcodes('html', true, true);
// 		}

		$text = preg_replace($hide_search_ary, '<div class="bb3hide">$1</div>', $text);

		$event['text']=$text;
	}

	public function bb3hide_bbtag($event)
	{
		$custom_tags=$event['custom_tags'];

		$helpline='';
		$mode=$this->request->variable('mode', '');
		if($mode=='compose')
		{
			$custom_tags['BBCODE_TAG_CLEAN']=$custom_tags['BBCODE_TAG'].'" style="display: none';

			$custom_tags['BBCODE_HELPLINE']=$custom_tags['A_BBCODE_HELPLINE']=$helpline;

			$event['custom_tags']=$custom_tags;
		}
		else
		{
			if($custom_tags['BBCODE_TAG']=='hide')
			{
				if($this->config['bb3hide_hide'] && $this->config['bb3hide_hideplus'])
				{
					$helpline=$this->user->lang['BB3HIDE_HIDES_HELPLINE'];
				}
				else if($this->config['bb3hide_hide'])
				{
					$helpline=$this->user->lang['BB3HIDE_HIDE_HELPLINE'];
				}
				else if($this->config['bb3hide_hideplus'])
				{
					$helpline=$this->user->lang['BB3HIDE_HIDEPLUS_HELPLINE'];
				}
				else
				{
					$custom_tags['BBCODE_TAG_CLEAN']=$custom_tags['BBCODE_TAG'].'" style="display: none';
				}
				$custom_tags['BBCODE_HELPLINE']=$custom_tags['A_BBCODE_HELPLINE']=$helpline;

				$event['custom_tags']=$custom_tags;
			}
			else if($custom_tags['BBCODE_TAG']=='uhide')
			{
				if(!$this->config['bb3hide_uhide'])
				{
					$custom_tags['BBCODE_TAG_CLEAN']=$custom_tags['BBCODE_TAG'].'" style="display: none';
				}

				$event['custom_tags']=$custom_tags;
			}
			else if($custom_tags['BBCODE_TAG']=='ghide')
			{
				if($this->config['bb3hide_ghide'] && $this->config['bb3hide_ghideplus'])
				{
						$helpline=$this->user->lang['BB3HIDE_GHIDE_HELPLINE'].' '.$this->user->lang['BB3HIDE_GHIDEPLUS_HELPLINE'];
				}
				else if($this->config['bb3hide_ghide'])
				{
					$helpline=$this->user->lang['BB3HIDE_GHIDE_HELPLINE'];
				}
				else if($this->config['bb3hide_ghideplus'])
				{
					$helpline=$this->user->lang['BB3HIDE_GHIDEPLUS_HELPLINE'];
				}
				else
				{
					$custom_tags['BBCODE_TAG_CLEAN']=$custom_tags['BBCODE_TAG'].'" style="display: none';
				}

				$custom_tags['BBCODE_HELPLINE']=$custom_tags['A_BBCODE_HELPLINE']=$helpline;

				$event['custom_tags']=$custom_tags;
			}
		}
	}

	public function bb3hide_displaytext($event)
	{
		if(isset($event['hilit']))
		{
			$display_text_only=true;

			$uid=$event['row']['bbcode_uid'];
			$text=$event['row']['post_text'];

			$poster_id=$event['row']['poster_id'];
			$forum_id=$event['row']['forum_id'];
		}
		else
		{
			if(!count($this->posts_data))
			{
				return false;
			}

			$display_text_only=false;

			$uid=$event['uid'];
			$text=$event['text'];
			$poster_id=isset($this->posts_data[$uid][0]) ? $this->posts_data[$uid][0] : 0;
			$forum_id=isset($this->posts_data[$uid][1]) ? $this->posts_data[$uid][1]: 0;
		}
		if((isset($this->posts_data[$uid]) && !$display_text_only) || ($display_text_only && !empty($uid)))
		{
			$is_can_view_hide=!$this->auth->acl_get('m_edit', $forum_id) && $this->user->data['user_id'] != $poster_id/* && !$this->is_admod*/ ? false : true;

			if($display_text_only)
			{
				//message все совпадения, сохранение текста и значений в бб-коде
				$hide_search_ary = $this->bb3hide_get_hide_bbcodes('message', false);

				$r='';
			}
			else
			{
				//html все совпадения и сохранения
				$hide_search_ary = $this->bb3hide_get_hide_bbcodes('html', false);
			}
			$show_search_ary=$this->bb3hide_get_show_bbcodes_descr();

			$hide_found=false;
			foreach($hide_search_ary as $k=>$v)
			{
				$hide_search=$v;
				$preg=array();
				preg_match_all($hide_search, $text, $preg);

				if(isset($preg[0]) && count($preg[0]))
				{
					foreach($preg[0] as $id=>$p)
					{
						$replaced=false;

						if(!$is_can_view_hide)
						{
							if($k=='hide' && ($this->user->data['user_id'] == ANONYMOUS || $this->user->data['is_bot'] == 1))
							{
								$replaced=true;
									$display_text_only ? '' : $r='<div class="bb3hide">' . sprintf($this->user->lang['BB3HIDE_REGISTER_GUEST'], $this->register_link) . '</div>';
								$text=str_replace($p, $r, $text);
							}
							else if($k=='hide=')
							{
								if(!is_array($this->posters_groups))
								{
									$this->posters_groups=array();
									$sql = 'SELECT group_id, user_id
										FROM ' . USER_GROUP_TABLE . '
										WHERE ' . $this->db->sql_in_set('user_id', $this->user->data['user_id']) . '
										AND user_pending = 0';
									$result = $this->db->sql_query($sql);
									while($sql_row = $this->db->sql_fetchrow($result))
									{
										$this->posters_groups[$sql_row['user_id']][$sql_row['group_id']] = $sql_row['group_id'];
									}
									$this->db->sql_freeresult($result);
								}
								$ignore_limit = false;
								foreach($this->posters_groups[$this->user->data['user_id']] as $poster_group)
								{
									if(in_array($poster_group, $this->ignore_groups))
									{
										$ignore_limit = true;
										break;
									}
								}
								if(!$ignore_limit)
								{
									if(!is_array($this->posters_posts))
									{
										if(!is_array($this->posters_defgroups))
										{
											$this->posters_defgroups=array();
										}
										$this->posters_posts=array();
										$sql = 'SELECT user_posts, user_id, group_id
											FROM ' . USERS_TABLE . '
											WHERE ' . $this->db->sql_in_set('user_id', $this->posts_posters);
										$result = $this->db->sql_query($sql);
										while($sql_row = $this->db->sql_fetchrow($result))
										{
											$this->posters_posts[$sql_row['user_id']] = $sql_row['user_posts'];
											$this->posters_defgroups[$sql_row['user_id']] = $sql_row['group_id'];
										}
										$this->db->sql_freeresult($result);
									}

									$posts=$preg[1][$id];

									$hide_cause = false;
									if($this->user->data['user_id'] == ANONYMOUS || $this->user->data['is_bot'] == 1)
									{
										if($posts == 0)
										{
													$hide_cause = $this->user->lang['BB3HIDE_REGISTER_GUEST'];
										}
										else
										{
													$hide_cause = sprintf($this->user->lang['BB3HIDE_POSTS_GUEST'], $this->register_link, $posts);
										}
									}
									else if($this->user->data['user_posts'] < $posts)
									{
										$hide_cause = sprintf($this->user->lang['BB3HIDE_POSTS'], $posts);
									}
									if($hide_cause)
									{
										$replaced=true;
										$display_text_only ? '' : $r='<div class="bb3hide">' . $hide_cause . '</div>';
										$text = str_replace($p, $r, $text);
									}
								}
							}
							else if($k=='uhide=')
							{
								$users_ary = explode(',', $preg[1][$id]);
								$uhide = true;
								foreach ($users_ary as $user_id)
								{
									if ($user_id==$this->user->data['user_id'])
									{
										$uhide = false;
										break;
									}
								}
								if($uhide)
								{
									$replaced=true;
									$display_text_only ? '' : $r='<div class="bb3hide">' . $this->user->lang['BB3HIDE_USERS'] . '</div>';
									$text=str_replace($p, $r, $text);
								}
							}
							else if(in_array($k, array('ghide', 'ghide=')))
							{
								if($k=='ghide')
								{
									if(!is_array($this->posters_defgroups))
									{
										if(!is_array($this->posters_posts))
										{
											$this->posters_posts=array();
										}
										$this->posters_defgroups=array();
										$sql = 'SELECT user_posts, user_id, group_id
											FROM ' . USERS_TABLE . '
											WHERE ' . $this->db->sql_in_set('user_id', $this->posts_posters);
										$result = $this->db->sql_query($sql);
										while($sql_row = $this->db->sql_fetchrow($result))
										{
											$this->posters_defgroups[$sql_row['user_id']] = $sql_row['group_id'];
											$this->posters_posts[$sql_row['user_id']] = $sql_row['user_posts'];
										}
										$this->db->sql_freeresult($result);
									}
	// 								if(!in_array($this->posters_defgroups[$poster_id], $this->posters_groups[$this->user->data['user_id']]))
									if($this->posters_defgroups[$poster_id]!=$this->posters_defgroups[$this->user->data['user_id']])
									{
										$replaced=true;
										$display_text_only ? '' : $r='<div class="bb3hide">' . $this->user->lang['BB3HIDE_GROUP'] . '</div>';
										$text = str_replace($p, $r, $text);
									}
								}
								else if($k=='ghide=')
								{
									if(!is_array($this->posters_groups))
									{
										$this->posters_groups=array();
										$sql = 'SELECT group_id, user_id
											FROM ' . USER_GROUP_TABLE . '
											WHERE ' . $this->db->sql_in_set('user_id', $this->user->data['user_id']) . '
											AND user_pending = 0';
										$result = $this->db->sql_query($sql);
										while($sql_row = $this->db->sql_fetchrow($result))
										{
											$this->posters_groups[$sql_row['user_id']][$sql_row['group_id']] = $sql_row['group_id'];
										}
										$this->db->sql_freeresult($result);
									}

									$groups=explode(',', $preg[1][$id]);

									$ghide = true;
									foreach($groups as $group)
									{
										if(in_array(intval($group), $this->posters_groups[$this->user->data['user_id']]))
										{
											$ghide = false;
											break;
										}
									}
									if($ghide)
									{
										$replaced=true;
										$display_text_only ? '' : $r='<div class="bb3hide">' . $this->user->lang['BB3HIDE_GROUPS'] . '</div>';
										$text = str_replace($p, $r, $text);
									}
								}
							}
						}
						if(!$replaced)
						{
							$replaced=true;
							$text_key=isset($preg[3][$id]) ? 3 : (isset($preg[2][$id]) ? 2 : 1);
							$value_key=1;

							$hidden_text_descr=preg_replace('#<a href="%s" class="postlink">(.*?)</a>#', '\\1', $show_search_ary[$k]);
							preg_match('#%(s|d)#', $hidden_text_descr) ? $hidden_text_descr=sprintf($hidden_text_descr, $preg[$value_key][$id]) : '';
							$display_text_only ? '' : $r='<div class="bb3hide" title="'.strip_tags($hidden_text_descr).'">' . $preg[$text_key][$id] . '</div>';
							$text = str_replace($p, $r, $text);
						}
						if($replaced)
						{
							$hide_found=true;
						}
					}
				}
			}
			if($hide_found)
			{
				if($display_text_only)
				{
					$row=$event['row'];
					$row['post_text']=$text;
					$event['row']=$row;
				}
				else
				{
					$event['text']=$text;
				}
			}
		}

	}

	public function bb3hide_add_lang($event)
	{
		$lang_set_ext=$event['lang_set_ext'];

		$lang_set_ext[]=array(
			'ext_name' => 'ppk/bb3hide',
			'lang_set' => 'bb3hide',
		);

		$event['lang_set_ext']=$lang_set_ext;
	}

	public function bb3hide_init()
	{
		$this->is_admod=$this->auth->acl_gets('a_', 'm_') || $this->auth->acl_getf_global('m_') ? 1 : 0;
		$this->ignore_groups = explode(',', $this->config['bb3hide_ignorelimit_groups']);

		$this->register_link=append_sid("{$this->root_path}ucp.{$this->php_ext}", 'mode=register');
	}

	public function bb3hide_get_show_bbcodes_descr()
	{
		if(is_array($this->show_search_ary))
		{
			return $this->show_search_ary;
		}
		$this->show_search_ary=array();

		if($this->config['bb3hide_hide'])
		{
			$this->show_search_ary['hide'] = $this->user->lang['BB3HIDE_REGISTER'];
		}
		if($this->config['bb3hide_hideplus'])
		{
			$this->show_search_ary['hide='] = $this->user->lang['BB3HIDE_POSTS'];
		}
		if($this->config['bb3hide_uhide'])
		{
			$this->show_search_ary['uhide='] = $this->user->lang['BB3HIDE_USERS'];
		}
		if($this->config['bb3hide_ghide'])
		{
			$this->show_search_ary['ghide'] = $this->user->lang['BB3HIDE_GROUP'];
		}
		if($this->config['bb3hide_ghideplus'])
		{
			$this->show_search_ary['ghide='] = $this->user->lang['BB3HIDE_GROUPS'];
		}

		return $this->show_search_ary;
	}

	public function bb3hide_get_hide_bbcodes($type='bbcode', $text_only=true, $optional=false)
	{
		$hide_search_ary=array();

		$bbcode_constructor=array(
			'bb3hide_hide' => array(
				'bbcode'=>'hide',
				'value'=>'',
				'values'=>array(),
				'text'=>'(.*?)',
				'regexp_option'=>'',
			),
			'bb3hide_hideplus' => array(
				'bbcode'=>'hide=',
				'value'=>'[0-9]+',
				'values'=>array(),
				'text'=>'(.*?)',
				'regexp_option'=>'',
			),
			'bb3hide_uhide' => array(
				'bbcode'=>'uhide=',
				'value'=>'[,0-9]+',
				'values'=>array(),
				'text'=>'(.*?)',
				'regexp_option'=>'',
			),
			'bb3hide_ghide' => array(
				'bbcode'=>'ghide',
				'value'=>'',
				'values'=>array(),
				'text'=>'(.*?)',
				'regexp_option'=>'',
			),
			'bb3hide_ghideplus' => array(
				'bbcode'=>'ghide=',
				'value'=>'[,0-9]+',
				'values'=>array(),
				'text'=>'(.*?)',
				'regexp_option'=>'',
			),
		);

		foreach($bbcode_constructor as $config_name => $bbcode_ary)
		{
			if($this->config[$config_name])
			{
				$bbcode_md5=$config_name.$type.$text_only.$optional;
				if(isset($this->hide_search_ary[$bbcode_md5]))
				{
					$hide_search_ary+=$this->hide_search_ary[$bbcode_md5];
					continue;
				}

				$bbcode_start=$bbcode_ary['bbcode'];
				$value=$bbcode_ary['value'];
				$regexp_option=$bbcode_ary['regexp_option'];

				$bbcode_end=str_replace('=', '', $bbcode_start);
				if($value==='')
				{
					$quoted_value='""';
					$value='';
				}
				else
				{
					$quoted_value='"('.($text_only || $type=='message' ? '?:' : '').$value.')"';
					$value='('.($text_only ? '?:' : '').$value.')';
				}
				$text=$bbcode_ary['text'];
				$bbcode_upper=strtoupper($bbcode_end);
				$quoted_values=$values='';
				if($bbcode_ary['values'])
				{
					foreach($bbcode_ary['values']['key'] as $values_id => $values_key)
					{
						$values_value=$bbcode_ary['values']['value'][$values_id];
						$values_optional=$bbcode_ary['values']['optional'][$values_id];

						if($values_value==='')
						{
							$quoted_values_string='""';
							$values_string='';
						}
						else
						{
							$quoted_values_string='('.($text_only || $type=='message' ? '?:' : '').$values_value.')'.($optional && $values_optional ? '?' : '');
							$values_string='('.($text_only ? '?:' : '').$values_value.')'.($optional && $values_optional ? '?' : '');
						}

						$quoted_values.=' '.($type=='html' ? 'data-hide-' : '').$values_key.'="'.$quoted_values_string.'"';
						$values.=' '.($type=='html' ? 'data-hide-' : '').$values_key.'='.$values_string;
					}
				}
				if($type=='bbcode')
				{
					$hide_search_ary[$bbcode_start] = $this->hide_search_ary[$bbcode_md5][$bbcode_start] = '!\['.$bbcode_start.$value.$values.'\]'.$text.'\[/'.$bbcode_end.'\]!'.$regexp_option.'s';
				}
				else if($type=='html')
				{
					$hide_search_ary[$bbcode_start] = $this->hide_search_ary[$bbcode_md5][$bbcode_start] = '!<div class="adapthide" data-hide-name="'.$bbcode_end.'" data-hide-value='.$quoted_value.$quoted_values.'>'.$text.'</div>!'.$regexp_option.'s';
				}
				else if($type=='message')
				{
					$hide_search_ary[$bbcode_start] = $this->hide_search_ary[$bbcode_md5][$bbcode_start] = '!<'.$bbcode_upper.$quoted_values.($value ? ' '.$bbcode_start.$quoted_value : '').'><s>\['.$bbcode_start.$value.$values.'\]</s>'.$text.'<e>\[/'.$bbcode_end.'\]</e></'.$bbcode_upper.'>!'.$regexp_option.'s';
				}
			}
		}

		return $hide_search_ary;
	}

}
