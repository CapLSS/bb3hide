<?php
/**
* @package BB3Hide
* @copyright (c) 2018 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

namespace ppk\bb3hide\migrations;

class bb3hide_manage3 extends \phpbb\db\migration\migration
{
	public function effectively_installed()
	{
		return isset($this->config['bb3hide_version']) && version_compare($this->config['bb3hide_version'], '1.3.0', '>=');
	}

	static public function depends_on()
	{
		return array('\ppk\bb3hide\migrations\bb3hide_manage2');
	}

	public function update_data()
	{
		return array(
			array('config.update', array('bb3hide_version', '1.3.6')),

			array('custom', array(array($this, 'install_bb3hide_bbcode'))),
		);
	}

	public function install_bb3hide_bbcode()
	{

		$bbcode_data = array(
			'hide=' => array(
				'bbcode_helpline'	=> '',
				'bbcode_match'		=> '[hide={NUMBER;optional}]{TEXT}[/hide]',
				'bbcode_tpl'		=> '<div class="adapthide" data-hide-name="hide" data-hide-value="{NUMBER}">{TEXT}</div>',
			),

		);

		global $db, $request, $user;
		$acp_manager = new \ppk\bb3hide\includes\acp_manager($db, $request, $user, $this->phpbb_root_path, $this->php_ext);
		$acp_manager->install_bbcodes($bbcode_data);
	}
}
