<?php
/**
* @package BB3Hide
* @copyright (c) 2020 PPK
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*/

namespace ppk\bb3hide\migrations;

class bb3hide_manage4 extends \phpbb\db\migration\migration
{
	public function effectively_installed()
	{
		return isset($this->config['bb3hide_version']) && version_compare($this->config['bb3hide_version'], '1.3.7', '>=');
	}

	static public function depends_on()
	{
		return array('\ppk\bb3hide\migrations\bb3hide_manage3');
	}

	public function update_data()
	{
		return array(
			array('config.add', array('bb3hide_hide', '1')),

			array('config.update', array('bb3hide_version', '1.3.7')),

		);
	}

	public function revert_data()
	{
		return array(
			array('config.remove', array('bb3hide_hide')),
		);
	}

}
