<?php

/**
*
* @package BB3Hide
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(

	'BB3HIDE' => 'BB3Hide',
	'BB3HIDE_EXPLAIN' => 'Hide text of messages from guests and bots, by the number of messages or user group',

	'BB3HIDE_HIDE' => 'Hide text from guests and bots',
	'BB3HIDE_HIDE_EXPLAIN' => 'Enable the ability to hide text from guests and bots, example:
	<br /> [hide] text [/ hide]
	<br /> the text will be visible to everyone except guests and bots',
	'BB3HIDE_HIDEPLUS' => 'Hide text by number of messages',
	'BB3HIDE_HIDEPLUS_EXPLAIN' => 'Enable the ability to hide text by the number of messages, example:
		<br /> [hide=100]text[/hide]
		<br /> the text will be visible only to users with the number of forum posts equal to or greater than 100 ',
	'BB3HIDE_GHIDE' => 'Hide text by user group',
	'BB3HIDE_GHIDE_EXPLAIN' => 'Enable the ability to hide text by default user group, example:
		<br /> [ghide]text[/ghide]
		<br /> the text will be visible only to users with the same default group as the author of the message ',
	'BB3HIDE_UHIDE' => 'Hide text by user ID',
	'BB3HIDE_UHIDE_EXPLAIN' => 'Enable the ability to hide text by user ID, example:
		<br /> [uhide=5,200]text[/uhide]
		<br /> the text will be visible only to users with IDs 5 and 200 ',
	'UHIDE_USERID' => 'By user ID',
	'BB3HIDE_GHIDEPLUS' => 'Hide text by group ID',
	'BB3HIDE_GHIDEPLUS_EXPLAIN' => 'Enable the ability to hide text by group ID, example:
		<br /> [ghide=4,5]text[/ghide]
		<br /> the text will be visible only to users from groups 4 (Super Moderators) and 5 (Administrators) ',
	'BB3HIDE_IGNORELIMIT_GROUPS' => 'Ignored groups',
	'BB3HIDE_IGNORELIMIT_GROUPS_EXPLAIN' => 'Members of these groups will see hidden text regardless of the number of messages',
));
