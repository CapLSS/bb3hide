<?php

/**
*
* @package BB3Hide
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'BB3HIDE_CONFIG' => 'Settings',
	'BB3HIDE_STANDART_BBCODES' => 'Standard BB codes',

	'BB3HIDE_REGISTER_GUEST' => '<i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;<i> Hidden text. You must <a href="%s" class="postlink"> register </a> </i>',
	'BB3HIDE_POSTS_GUEST' => '<i class="fa fa-commenting" aria-hidden="true"></i>&nbsp;<i> Hidden text. You must be <a href="%s" class="postlink"> registered </a> and have messages: %d </i>',

	'BB3HIDE_REGISTER' => '<i class="fa fa-sign-in" aria-hidden="true"> </i> &nbsp; <i> Hidden text. To view you need to register </i> ',
	'BB3HIDE_POSTS' => '<i class="fa fa-commenting" aria-hidden="true"></i>&nbsp;<i> Hidden text. Must have posts: %d </i>',
	'BB3HIDE_USERS' => '<i class="fa fa-user-secret" aria-hidden="true"></i>&nbsp;<i> Hidden text. Only available to specific users </i>',
	'BB3HIDE_GROUP' => '<i class="fa fa-user" aria-hidden="true"></i>&nbsp;<i> Hidden text. You are not in the default group to which this text is available </i>',
	'BB3HIDE_GROUPS' => '<i class="fa fa-users" aria-hidden="true"></i>&nbsp;<i> Hidden text. You are not a member of the groups to which this text is available </i>',

	'BB3HIDE_HIDDEN_TEXT' => 'Hidden text',
	'BB3HIDE_QUOTE' => '[i] Hidden text [/i]',
	'BB3HIDE_QUOTE_PREVIEW' => '<i> Hidden Text </i>',

	'BB3HIDE_HIDES_HELPLINE' => 'Hide text from guests and bots: [hide]text[/hide], hide text from users with less messages than [hide=number]text[/hide]',
	'BB3HIDE_HIDE_HELPLINE' => 'Hide text from guests and bots: [hide]text[/hide]',
	'BB3HIDE_HIDEPLUS_HELPLINE' => 'Hide text from users with less than the specified message [hide=number]text[/hide]',
	'BB3HIDE_GHIDE_HELPLINE' => 'Hide text from users outside your group by default: [ghide]text[/ghide]',
	'BB3HIDE_GHIDEPLUS_HELPLINE' => 'Hide text from users from outside the specified groups [ghide=group_number_1,group_number_2]text[/ghide]',
	'BB3HIDE_UHIDE_HELPLINE' => 'Hide text from other users (by ID): [uhide=1,2,3]text[/uhide] (will be visible only to users with the specified IDs)',


));
